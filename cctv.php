<?php

header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header('Content-Type: application/json');

$cache_file = "cctv.dat";

if (!file_exists($cache_file) || filemtime($cache_file) < time() - 60 * 5) {
	$body = '{"arguments":"SAT,30.058314841883174,-99.89972143709285,28.873205082112975,-97.0339377456866"}';
	$opts = array('http' =>
		array(
			'method'  => 'POST',
			'header'  => "Content-type: application/json; charset=UTF-8\r\n",
			'content' => $body,
			'timeout' => 15
		)
	);
	$context  = stream_context_create($opts);
	$url = 'http://www.transguide.dot.state.tx.us/ITS_WEB/Frontend/svc/DataRequestWebService.svc/GetCctvDataOfArea';
	$data = file_get_contents($url, false, $context, -1, 40000);
	file_put_contents($cache_file, $data);
} else {
	$data = file_get_contents($cache_file);
}

$data = explode(',', trim($data, '"'));
$data = array_slice($data, 6);

$json = array();

while ($data) {
	list($name, $id, $status, $district, $lat, $lon, $img, $time, $_, $hwy, $hwydirection, $speed_count) = array_slice($data, 0, 12);
	//var_dump(array_slice($data, 0, 12));
	$data = array_slice($data, 12);
	if ($speed_count != '0') {
		// Each speed datum has 3 elements. Skip over them
		$data = array_slice($data, intval($speed_count) * 3);
	}

	$cam_data = array('url' => $id, 'name' => $name, 'online' => strpos($status, 'Online') !== false);
	array_push($json, $cam_data);
}

echo 'var cctvs='.json_encode($json);
