<?php

header('Expires: Sun, 01 Jan 2014 00:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
header('Content-Type: image/jpeg');

$id = $_GET["id"];
if (!isset($id)) {
	$id = 'CCTC-0410W-021.121_SAT';
}

$body = '{"arguments":"'.$id.',1"}';
$opts = array('http' =>
	array(
		'method'  => 'POST',
		'header'  => "Content-type: application/json; charset=UTF-8\r\n",
		'content' => $body,
		'timeout' => 15
	)
);
$context  = stream_context_create($opts);
$url = 'http://its.txdot.gov/ITS_WEB/Frontend/svc/DataRequestWebService.svc/GetCctvContent';
$data = file_get_contents($url, false, $context, -1, 400000);

$data = explode(',', trim($data, '"'));
echo base64_decode($data[4]);
